package HillelRepit.com.maydukov.lecture001.homework00;

public class HelloWorld {
    public static void main(String[] args) {
        prettyMethod();
    }

    private static void prettyMethod() {
        System.out.println("Hello World");
        System.out.println("Пешется для теста как работают коммиты");
        System.out.println("Еще один коммит!!!");
    }
}
