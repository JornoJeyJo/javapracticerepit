package HillelRepit.com.maydukov.lecture003.homework03;

public class ChineseDynasties {
    public static void main(String[] args) {
        double allDamageLi = 860;
        double allDamageMin = allDamageLi * 1.5;
        int liWarriors = 13;
        int liArchers = 24;
        int liRiders = 46;
        double liDynastyArmy = allDamageLi * (liWarriors + liArchers + liRiders);


        int minWarriors = 9;
        int minArchers = 35;
        int minRiders = 12;
        double minDynastyArmy = allDamageMin * (minWarriors + minArchers + minRiders);


        System.out.println("Суммарная атака династии Ли равняется: " + liDynastyArmy);
        System.out.println("Суммарная атака динатсии Мин равняется: " + minDynastyArmy);

        if (minDynastyArmy < liDynastyArmy) {
            System.out.println("Победила Династия Ли");
        } else if (minDynastyArmy > liDynastyArmy) {
            System.out.println("Победила Династия Мин");
        } else {
            System.out.println("Ничья");
        }
    }
}
