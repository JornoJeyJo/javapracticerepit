package HillelRepit.com.maydukov.lecture003.homework04;

public class CounterStrike {
    public static void main(String[] args) {
        int numberOfPlayers = 5;

        String policeTeam = "Counter terrorists";
        int firstPolice = 12;
        int secondPolice = 49;
        int thirdPolice = 39;
        int fourthPolice = 54;
        int fifthPolice = 11;
        int allFragsPoliceTeam = firstPolice + secondPolice + thirdPolice + fourthPolice + fifthPolice;
        double policeCoins = allFragsPoliceTeam / numberOfPlayers;


        String terroristTeam = "Terrorists";
        int firstTerrorist = 7;
        int secondTerrorist = 90;
        int thirdTerrorist = 21;
        int fourthTerrorist = 3;
        int fifthTerrorist = 9;
        int allFragsTerroristTeam = firstTerrorist + secondTerrorist + thirdTerrorist + fourthTerrorist + fifthTerrorist;
        double terroristCoins = allFragsTerroristTeam / numberOfPlayers;


        System.out.println(policeTeam + " набрали: " + policeCoins);
        System.out.println(terroristTeam + " набрали: " + terroristCoins);


        double precision = 0.000001;
        if (Math.abs(terroristCoins - policeCoins) < precision) {
            System.out.println("Обе команды одержали ничью: " + terroristTeam);
        } else if (terroristCoins > policeCoins) {
            System.out.println("Команда: " + terroristTeam + " Победила. Набрав: " + terroristCoins + "Очков");
        } else {
            System.out.println("Команда: " + policeTeam + " Победила. Набрав: " + policeCoins + "Очков");
        }
    }
}
