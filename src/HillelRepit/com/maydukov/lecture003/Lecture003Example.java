package HillelRepit.com.maydukov.lecture003;

public class Lecture003Example {
    public static void main(String[] args) {

        /*mathOperations();*/
        /*conditions();*/
        ternaryOperator();


    }

    private static void ternaryOperator() {
        int result;
        if (10 > 8) {
            result = 100;
        } else {
            result = -42;
        }
        System.out.println(result);

        result = 10 > 8 ? 100 : -42; // тернарная проверка значений.
        System.out.println(result);
    }

    private static void conditions() {
        boolean flag = true;
        int age = 14;
        int height = 125;
        if (age > 5 && age < 18 && height > 100 && height < 138) {
            System.out.println("Young and true height");
        } else {
            System.out.println("Error");
        }
/*        if (age < 0 | age > 100) {
            System.out.println("Error");
        } else {
            if (age > 42) {
                System.out.println("Adult");
            } else {
                System.out.println("Young");
            }
        }*/
    }

    private static void mathOperations() {
        int a = 10;
        int b = 3;

        System.out.println("a / b = " + (a / b));
        int mod = a - (b * (a / b));
        System.out.println("Mod: " + mod);
        int trueMod = a % b;
        System.out.println("True mod: " + trueMod);
    }

    private static void increments() {
        int myPrettyLongValue = 1;
        System.out.println("Инкременты: ");
        System.out.println(myPrettyLongValue);
        myPrettyLongValue += 1;
        System.out.println(myPrettyLongValue);
        myPrettyLongValue += 1;
        System.out.println(myPrettyLongValue);
        myPrettyLongValue += 2 * 4 + 5 / 3;
        System.out.println(myPrettyLongValue);

        myPrettyLongValue++;
        System.out.println(myPrettyLongValue);

        ++myPrettyLongValue;
        System.out.println(myPrettyLongValue);

        myPrettyLongValue = myPrettyLongValue++ + 10; // 24 , проикремент вначале добавляет единицу и после добавляет новое число.
        //25, если бы был преикримент, добавляет вначале значение а потом уже добавляет число.
        System.out.println(myPrettyLongValue);
        System.out.println(myPrettyLongValue);


        System.out.println("Декременты: ");
        System.out.println(myPrettyLongValue);
        myPrettyLongValue -= 1;
        System.out.println(myPrettyLongValue);
        myPrettyLongValue -= 1;
        System.out.println(myPrettyLongValue);
        myPrettyLongValue -= 2 * 4 + 5 / 3;
        System.out.println(myPrettyLongValue);

        myPrettyLongValue--;
        System.out.println(myPrettyLongValue);

        --myPrettyLongValue;
        System.out.println(myPrettyLongValue);

        myPrettyLongValue = myPrettyLongValue-- - 10;
        System.out.println(myPrettyLongValue);
    }
}
