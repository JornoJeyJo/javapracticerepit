package HillelRepit.com.maydukov.lecture004.homework05;

import java.util.Random;
import java.util.Scanner;

public class MyRandomNumber {
    public static void main(String[] args) {
        guessNumber();
    }

    public static void guessNumber() {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int randomNumber = random.nextInt(10) + 1;
        int allOverAttempts = 5;
        int attempt = 0;

        while (allOverAttempts > attempt) {
            System.out.println("Угадай число от 1 до 10 c помощью " + allOverAttempts + " попыток");
            System.out.println("Попытка номер: " + (attempt + 1) + ": ");
            int userNumber = scanner.nextInt();
            attempt++;

            if (userNumber == randomNumber) {
                System.out.println("Это секретный номер, вы выиграли!!!");
                break;
            } else {
                System.out.println("Нет, это не является загадным числом!");
                System.out.printf("У вас использовалась одна попытка, у вас осталось %d попыток \n", (allOverAttempts - attempt));
            }
            if (attempt == 5) {
                System.out.println("Вы проиграли");
            }
        }
    }
}
