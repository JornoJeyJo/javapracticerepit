package HillelRepit.com.maydukov.lecture004;

public class CarAnalyser {
    public static void main(String[] args) {
        /*forExample();*/
        /*whileExample();*/
        /*doWhileExample();*/
       /* int[] array = {1, 2, 3, 10};
        int[] anotherArray = {3, 5, 7, 9};

        int[] randomArray = generateArray(10);


        printArray(array);
        printArray(array);
        printArray(anotherArray);
        printArray(randomArray);*/


        enumExample();
    }

    private static void enumExample() {
        String[] numbers = new String[]{
                "AA1111AA",
                "AA2222AA",
                "AB3333AB",
                "AA4444AA",
                "AA5555AA",
                "BB6666BB",
                "AA7777AA",
                "AA8888AC"
        };

        for (String value : numbers) {
            /*if ("AA1111AA".equals(value)) {
                System.out.println("Привет Кирилл");
            } else if ("AA5555AA".equals(value)) {
                System.out.println("Твоя машина черная");
            } else {
                System.out.println("Неизвестный номер");
            }*/
            switch (value) {
                case "AA1111AA":
                    System.out.println("Привет Кирилл");
                    break;
                case "AA5555AA":
                    System.out.println("Твоя машина черная");
                    break;
                default:
                    System.out.println("Нет данных");
            }
        }
    }

   /* private static int[] generateArray(int size) {
        Random random = new Random();
        int[] result = new int[size];
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(10);
            *//*result[i] = ThreadLocalRandom.current().nextInt(1,10) 2-ой способ находить случайное значение;*//*
        }
        return result;
    }


    public static void printArray(int[] array) {
        System.out.println(" Я из метода:)");
        for (int i = 0; i < array.length; i++) {
            System.out.println(i + ": " + array[i]);
        }
    }
*/
    /*private static void doWhileExample() {
        String[] numbers = new String[]{
                "AA1111AA",
                "AA2222AA",
                "AB3333AB",
                "AA4444AA",
                "AA5555AA",
                "BB6666BB",
                "AA7777AA",
                "AA8888AC"

        };

        int count = 0;
        String pattern = "AA";
        int i = -1;

        do {

            if (numbers[i].contains(pattern)) {
                count++;
            } else {
                continue;
            }


            System.out.println(numbers[i]);
        }
        while ((i < numbers.length - 1));


        System.out.println("Найдено: " + count + " номеров");
    }*/

    /*private static void whileExample() {
        String[] numbers = new String[]{
                "AA1111AA",
                "AA2222AA",
                "AB3333AB",
                "AA4444AA",
                "AA5555AA",
                "BB6666BB",
                "AA7777AA",
                "AA8888AC"

        };

        int count = 0;
        String pattern = "AA";
        int i = -1;

        while (i < numbers.length - 1) {
            i++;
            if (numbers[i].contains(pattern)) {
                count++;
            } else {
                continue;
            }


            System.out.println(numbers[i]);
        }


        System.out.println("Найдено: " + count + " номеров");
    }*/

    /*private static void forExample() {
        String[] numbers = new String[]{
                "AA1111AA",
                "AA2222AA",
                "AB3333AB",
                "AA4444AA",
                "AA5555AA",
                "BB6666BB",
                "AA7777AA",
                "AA8888AC"

        };

        int count = 0;
        String pattern = "AA";

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i].contains(pattern)) {
                count++;
            } else {
                continue;
            }


            System.out.println(numbers[i]);
        }

       *//* for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(i * j + "\t");
            }
            System.out.println();
        }*//*
        System.out.println("Найдено: " + count + " номеров");
    }*/
}
