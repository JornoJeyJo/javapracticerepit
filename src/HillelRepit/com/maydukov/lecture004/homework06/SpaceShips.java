package HillelRepit.com.maydukov.lecture004.homework06;

public class SpaceShips {
    public static void main(String[] args) {
        String modelOfShip = "ZRH-0-";
        int count = 0;
        for (int i = 0; count < 100; i++) {
            if (containsUnluckyNumber(i)) {
                continue;
            }
            count++;
            System.out.println("Привет я космический корбаль: " + modelOfShip + (i));
        }
    }

    public static boolean containsUnluckyNumber(int num) {
        while (num > 0) {
            if (num % 10 == 4 || num % 10 == 9) {
                return true;
            }
            num /= 10;
        }
        return false;
    }
}
