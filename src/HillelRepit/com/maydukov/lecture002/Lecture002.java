package HillelRepit.com.maydukov.lecture002;

public class Lecture002 {
    public static void main(String[] args) {
        byte b = 127/*Константа*/;
        b = (byte) /*Приведения типов данных*/(b + 1);

        System.out.println(b);


        int i = 2_147_483_647;
        i = i + 1;

        System.out.println(i);


        double first = 42;
        double second = 5;

        double result = first + second;
        System.out.println("+: " + (int) (result));

        result = first - second;
        System.out.println("-: " + (int) (result));

        result = first / second;
        System.out.println("*: " + result);

        result = first * second;
        System.out.println("/: " + (int) (result));


        /*Способы записи чисел разными системами чисел. Пример: число 12*/
        i = 12;
        System.out.println(i);
        i = 0xC;
        System.out.println(i);
        i = 0b00001100;
        System.out.println(i);
        i = 014;
        System.out.println(i);
    }
}
